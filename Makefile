build: build/index.html build/patents.html build/music.html build/style.css
	mkdir -pv $@;

build/index.html:
	mkdir -pv $(dir $@);
	cp -v src/$(notdir $@) $@;

build/patents.html:
	mkdir -pv $(dir $@);
	cp -v src/$(notdir $@) $@;

build/music.html:
	mkdir -pv $(dir $@);
	cp -v src/$(notdir $@) $@;

build/style.css:
	mkdir -pv $(dir $@);
	cp -v src/$(notdir $@) $@;

clean:
	rm -rf build;

all: build